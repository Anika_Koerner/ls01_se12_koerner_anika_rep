
public class KonsolenAusgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Aufgabe 1
		System.out.println("Die Sonne scheint.");
		System.out.println("Die Sonne scheint nicht.");
		//modifizierte S�tze
		System.out.println("Er sagte: \" Die Sonne scheint!\"");
		int alter = 18;
		String name = "Anika";
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahre alt.");
		//print() sorgt daf�r, dass alle Zeilen direkt hintereinander weg in der Console erscheinen
		//println() sorgt daf�r, dass Zeilenumbruche und Abst�nde genutzt werden
		//Aufgabe2
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***");
//Aufgabe3
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		System.out.printf("22.4234234 |%.2f|\n", a);
		System.out.printf("111.2222 |%.2f|\n", b);
		System.out.printf("4.0 |%.2f|\n", c);
		System.out.printf("1000000.551 |%.2f|\n", d);
		//System.out.printf("97.34 |%.2|\n", e);
	}

}
